import { Vue } from 'vue-property-decorator';
import { ValidationProvider } from "vee-validate";

export interface Rules {
    [key: string]: any
}

type ProviderInstance = InstanceType<typeof ValidationProvider>;

export interface VeeValidateObserverRef extends Vue {
    subscribe(subscriber: any, kind?: string): void;
    unsubscribe(id: string, kind?: string): void;
    validate(options?: { silent?: boolean }): Promise<boolean>;
    reset(): void;
    restoreProviderState(provider: ProviderInstance): void;
    removeProvider(id: string): void;
    setErrors(errors: Record<string, string[]>): void;
}
